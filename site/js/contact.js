
var messages = {
		contactname  	: "Enter your name",
		contactemail 	: "Enter your email",
		contactcity  	: "Enter your city (optional)",
		contactmessage 	: "Enter your text here..."
};

var emailValidationPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ 

$(document).ready(function() {
	
	// Init texts 
	$("#postcard input, #postcard textarea").each(function () {
		$(this).val(messages[$(this).attr('name')]);
	});
	
	// Delete text when texfield gains focus
	$("#postcard input, #postcard textarea").focus(function () {
		if ($(this).val() == messages[$(this).attr('name')]) $(this).val('');
	});

	// Restore default text when texfield looses focus and the user hasn't entered anything
	$("#postcard input, #postcard textarea").blur(function () {
		if ($(this).val() == '') $(this).val(messages[$(this).attr('name')]);
	});
});


function contactSubmit()
{
	var name =    $("#postcard *[name='contactname']").val();
	if (name == messages['contactname']) name = '';
	
	var email =   $("#postcard *[name='contactemail']").val();
	if (email == messages['contactemail']) email = '';

	var city =    $("#postcard *[name='contactcity']").val();
	if (city == messages['contactcity']) city = '';
	
	var message = $("#postcard *[name='contactmessage']").val();
	if (message == messages['contactmessage']) message = '';
	
	if (name == '') 
	{
		alert ('Please enter a name');
		return;
	}
	
	if (!email.match(emailValidationPattern)) 
	{
		alert ('Please enter a valid email address');
		return;
	}
	
	if (message == '') 
	{
		alert ('Please enter a message');
		return;
	}
	
	subject = "New message from "+name+(city!=''?" from "+city:"");
	
	$.ajax({
		type: "POST",
		url: "/scripts/sendmail.php",
		data: "subject="+subject+"&from="+email+"&body="+message,
		success: function() {
			alert("Thank you! Your message has been sent.");
			$("#postcard input, #postcard textarea").each(function () {
		                $(this).val(messages[$(this).attr('name')]);
		        });
		},
		error: function() {
			alert("Sorry, but your email could not be sent. Please try again later.");
		}
	});
}
