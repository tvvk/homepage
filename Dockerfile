FROM php:7.0-apache
COPY site/ /var/www/html/

RUN apt-get update \
 && apt-get install -y sendmail \
 && echo 'sendmail_path = "/usr/sbin/sendmail -t"' > /usr/local/etc/php/conf.d/mail.ini

CMD service sendmail restart && apache2-foreground
